/*
 * Main
 * Snapshot 1.0
 */

import java.util.Scanner;
import controller.Controller;
import fileProvider.FileProvider;
import model.collection.*;
import model.factory.*;

public class Main {

    private static final String filename="data.txt";
    private static final String dir="D://";

    public static void main(String[] args){

        Library lib;
        Scanner scanner=new Scanner(System.in);
        Disk disc = Disk.getInstance();
        boolean out=true;

        while(out){

            System.out.println("Доступные команды: show lib, show disk," +
                    " create disk, add track to lib, add track to disk," +
                    " sort disk," +
                    "disk length, find track, save disk");

            String choice = scanner.nextLine();

            switch (choice){

                case "show lib":
                    lib =FileProvider.ReadFromFile(filename);
                    System.out.println("Библиотека");
                    System.out.println(lib);
                    break;

                case "show disk":
                    System.out.println("Диск: "+disc.getName());
                    System.out.println(disc);
                    break;

                case "create disk":
                    Controller.CreateDisk();
                    System.out.println("Успешно! \r\nДиск: \r\n"+disc.getName());
                    System.out.println(disc);
                    break;

                case "add track to lib":
                    Controller.AddTrackToLib(filename);
                    break;

                case "add track to disk":
                    disc = Disk.getInstance();
                    Controller.AddTrackToDisk(disc,filename,dir);
                    break;

                case "sort disk":
                    disc = Disk.getInstance();
                        Controller.DiskSort(disc);
                    break;

                case "find track":
                        Controller.FindTrack(filename);
                    break;

                case "save disk":
                    Controller.SaveDisk(disc,dir);
                    break;

                case "disk length":
                    Controller.SaveDisk(disc,filename);
                    Controller.CalculateCommonLength(disc);
                    break;

                case "exit":
                    out=false;
                    break;

                default:
                    System.out.println("не найдена команда");
            }
        }
    }
}
