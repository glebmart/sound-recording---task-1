package controller;

import fileProvider.FileProvider;
import model.collection.Disk;
import model.collection.Library;
import model.factory.ClassicTrackCreator;
import model.factory.ElectronicTrackCreator;
import model.factory.RockTrackCreator;
import model.factory.Track;
import java.util.*;

public class Controller {

    private static String title, peformer, album, genre;
    private static double length;
    private static  Scanner input = new Scanner(System.in);
    private static final String[] genres = {"Electronic","Classic","Rock"};

    public static void AddTrackToLib(String fileName){

        System.out.println("Введите название:");
        title=input.nextLine();
        //if()

        System.out.println("Введите исполнителя:");
        peformer=input.nextLine();

        System.out.println("Введите альбом:");
        album=input.nextLine();

        System.out.println("Введите продолжительность:");

        do{
            while (!input.hasNextDouble()){
                System.out.println("Неверный формат");
                input.next();
            }
            length = input.nextDouble();
        }while (length<=0);

        System.out.println("Введите жанр, из доступных: Electronic, Classic, Rock");
        genre=input.next();
        while (!Arrays.asList(genres).contains(genre)) {
            System.out.println("Неверный жанр, доступны: Electronic, Classic, Rock");
            genre=input.next();
        }

        Track track;

        switch (genre)
        {
            case "Classic":
                track = new ClassicTrackCreator().createNewTrack(title,peformer,album,length);
                break;

            case "Electronic":
                track = new ElectronicTrackCreator().createNewTrack(title,peformer,album,length);
                break;

            case "Rock":
                track = new RockTrackCreator().createNewTrack(title,peformer,album,length);
                break;

            default:
                track = new RockTrackCreator().createNewTrack(title,peformer,album,length);
        }

        FileProvider.WriteToLib(track,fileName);
    }

    public static void AddTrackToDisk(Disk disc, String filename, String dir){

        if (!disc.isEmpty()||!disc.getName().equals("no name")){

            Library lib =FileProvider.ReadFromFile(filename);
            lib.print();
            System.out.println("Какой трек хотите добавить?");
            int num=input.nextInt();
            while (num<=0||num>lib.size()){
                System.out.println("Неверный номер трека, повторите попытку  "+lib.size());
                num=input.nextInt();
            }
            disc.add(lib.get(num-1));
            FileProvider.WriteToDisc(disc,dir);

        }
        else System.out.println("Сначала создайте диск");


    }

    public static void CalculateCommonLength(Disk disc){
        double length=0;
        for(Track track: disc){
            length+=track.getLength();
        }
        System.out.println("Продолжительность диска: "+length+" сек" );
    }

    public static void DiskSort(Disk disc){

        Collections.sort(disc, (Track o1, Track o2)->{
                if(o1.getPerformer().compareTo(o2.getPerformer())==0)
                    return o1.getAlbum().compareTo(o2.getAlbum());
                return o1.getPerformer().compareTo(o2.getPerformer());
        });
    }

    public static void SaveDisk(Disk disc,String dir){
        if (!disc.isEmpty()){
            FileProvider.WriteToDisc(disc,dir);
            System.out.println("Успешно!");
            disc.resetInstance();// Как удалить обьект?
        }
        else System.out.println("Диск пуст! Невозможно сохранить.");
    }

    public static void FindTrack(String filename){
        Library lib =FileProvider.ReadFromFile(filename); // инициализировать библиотеку
        System.out.println("Введите название для поиска...");

        int i=lib.indexOf(input.next());
        if(i==-1)
            System.out.println("Не найдено совпадений");
        else
        System.out.println("Есть совпадение:"+"\r\n"+"#"+i+"  "+lib.get(i));
    }

    public static void CreateDisk(){
        System.out.println("Введите название сборника:");
        Disk disc = Disk.getInstance(input.nextLine());
        System.out.println("Диск:"+disc+"\r\n");
    }

}
