package model.factory;

import model.Music;

class ClassicMusic extends Music implements Track{
    private static final String GENRE="Classic";
    ClassicMusic(String title, String performer,String album, double length) {
        super(title,album, performer,GENRE, length);
    }
}
