package model.factory;

public class RockTrackCreator extends Creator {
        @Override
        public Track createNewTrack(String title, String album, String performer, double length) {
            return new RockMusic(title,performer,album , length);
        }

    }
