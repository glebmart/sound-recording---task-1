package model.factory;

import model.Music;

class RockMusic extends Music implements Track{
    private static final String GENRE="Rock";
    RockMusic(String title, String performer,String album, double length) {
        super(title,album, performer,GENRE, length);
    }
}
