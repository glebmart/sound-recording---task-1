package model.factory;

abstract class Creator{
    public abstract Track createNewTrack(String title, String performer,String album, double length);
}
