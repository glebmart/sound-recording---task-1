package model.factory;

public class ClassicTrackCreator extends Creator{
    @Override
    public Track createNewTrack(String title,String album, String performer, double length) {
        return new ClassicMusic(title,performer,album , length);
    }
}
