package model.factory;

public interface Track {
     String toFileWrite();
     double getLength();
     String getTitle();
     String getPerformer();
     String getGenre();
     String getAlbum();
}

