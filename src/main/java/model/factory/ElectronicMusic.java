package model.factory;

import model.Music;

class ElectronicMusic extends Music implements Track{
    private static final String GENRE="Electronic";
    ElectronicMusic(String title, String performer,String album, double length) {
        super(title,album, performer,GENRE, length);
    }
}
