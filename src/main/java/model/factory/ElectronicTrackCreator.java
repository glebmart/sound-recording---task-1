package model.factory;

public class ElectronicTrackCreator extends Creator{
    @Override
    public Track createNewTrack(String title, String performer,String album, double length) {
        return new ElectronicMusic(title,performer,album , length);
    }
}
