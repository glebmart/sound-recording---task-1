package model.collection;

import model.factory.Track;
import java.util.ArrayList;

public class Library extends ArrayList<Track> {

    private static Library instance;

    private Library(){}

    public static Library getInstance()
    {
        Library localInstance = instance;
        if(localInstance==null){
            synchronized (Library.class){
                localInstance = instance;
                if(localInstance==null){
                    instance = localInstance = new Library();
                }

            }
        }
        return localInstance;
    }

    public void print(){
        int i=1;
        for(Track track : this){
            System.out.println("#"+i+"  "+track);
            i++;
        }
    }

    @Override
    public String toString(){
        StringBuilder buffer =new StringBuilder();
        for(Track track: this) {
            buffer.append("Title: " + track.getTitle() + ", " + "Performer: " + track.getPerformer()
                    + ", " + "Album: " + track.getAlbum() + ", " + "Length: " + track.getLength()
                    + ", " + "Genre: " + track.getGenre() + " \n\r");
        }
        return buffer.toString();
    }

    public int indexOf(String title) {

        for (int i=0; i<this.size();i++) {
            if (this.get(i).getTitle().equals(title))
                return i;
        }

        return -1;
    }

    public boolean contains(Track trackFind) {

        String find, ths;

        for (Track track: this){
            ths=track.getPerformer();
            find=trackFind.getPerformer();
           if(ths.equals(find)){
               return true;
           }
        }
        return false;
    }

}
