package model.collection;

import model.factory.Track;
import java.util.ArrayList;

public class Disk extends ArrayList<Track> {

    private String name;
    private static Disk instance;

    private Disk(){}

    public static Disk getInstance(){
        Disk localInstance = instance;
        if(localInstance==null){
            synchronized (Disk.class){
                localInstance = instance;
                if(localInstance==null){
                    instance = localInstance = new Disk();
                    localInstance.setName("no name");
                }
            }
        }
        return localInstance;
    }

    public static Disk getInstance(String name){
        Disk localInstance = instance;
        if(localInstance==null){
            synchronized (Disk.class){
                localInstance = instance;
                if(localInstance==null){
                    instance = localInstance = new Disk();
                    localInstance.setName(name);
                }
            }
        }
        localInstance.setName(name);
        return localInstance;
    }

    public void resetInstance(){
        instance = new Disk();
    }

    public String getName() {
        return name;
    }



    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        StringBuilder buffer =new StringBuilder();
        for(Track track: this) {
            buffer.append( "Title: " + track.getTitle() + ", " + "Performer: " + track.getPerformer()
                    + ", " + "Album: " + track.getAlbum() + ", " + "Length: " + track.getLength()
                    + ", " + "Genre: " + track.getGenre() + " \n\r");
        }
        return buffer.toString();
    }

}
