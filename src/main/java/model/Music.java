package model;


import model.factory.Track;

public class Music implements Comparable<Track> {
    private String title;
    private String performer;
    private String genre;
    private String album;
    private double length;

    @Override
    public int compareTo(Track o) {
        return 0;
    }

    public int compareTo(Music o){
        if(performer.compareTo(o.performer)==0)
        return album.compareTo(o.album);
        return performer.compareTo(o.performer);
    }


    public Music(String title, String performer, String album, String genre, double length) {
        if (title==null&&performer==null&&album==null&&length<=0)
        {
            throw new IllegalArgumentException("not valid input");
        }
        this.title = title;
        this.performer = performer;
        this.album = album;
        this.genre = genre;
        this.length = length;
    }

    public String getTitle() {
        return title;
    }

    public String getPerformer() {
        return performer;
    }

    public String getGenre() {
        return genre;
    }

    public String getAlbum() {
        return album;
    }

    public double getLength() {
        return length;
    }

    @Override
    public String toString() {
        return "Title: "+this.getTitle()+" , "+"Performer: "+this.getPerformer()
                +" , "+"Album: "+this.getAlbum()+" , "+"Length: "+this.getLength()
                +" , "+"Genre: "+this.getGenre()+" \n\r";
    }

    public String toFileWrite() {
        return this.getTitle()+","+this.getPerformer()+","+this.getAlbum()
                +","+this.getLength()+","+this.getGenre()+"\r";
    }


}


