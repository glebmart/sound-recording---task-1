package fileProvider;

import model.collection.*;
import model.factory.*;
import java.io.*;
import java.util.Scanner;

public class FileProvider {

    public static Library ReadFromFile(String filename){
        Library library= Library.getInstance();
        try(Scanner scan=new Scanner(new File(filename))){
            String[] part;
            String choice;
            Track track;
            while(scan.hasNextLine()&&scan.hasNext()){
                part=scan.nextLine().split(",");
                choice=part[4];
                switch (choice)
                {
                    case "Classic":
                        track =new ClassicTrackCreator().createNewTrack(part[0],
                                part[1],part[2],Double.parseDouble(part[3]));
                        break;

                    case "Electronic":
                        track =new ElectronicTrackCreator().createNewTrack(part[0],
                                part[1],part[2],Double.parseDouble(part[3]));
                        break;

                    case "Rock":
                        track =new RockTrackCreator().createNewTrack(part[0],
                                part[1],part[2],Double.parseDouble(part[3]));
                        break;

                    default:
                        track =new RockTrackCreator().createNewTrack(part[0],
                                part[1],part[2],Double.parseDouble(part[3]));
                }

                if(!library.contains(track))
                    library.add(track);
            }
        }catch (FileNotFoundException e){
            e.printStackTrace();
        }
        return library;
    }

    public static void WriteToDisc(Disk disc, String dir){
        String discName=dir+disc.getName()+".txt";
        try(FileWriter writer = new FileWriter(discName,false)) {

            for (Track track : disc){

                writer.write("\n"+track.toFileWrite());
            }
        }
        catch (IOException ex){
            System.out.println(ex.getMessage());
        }
    }

    public static void WriteToLib(Track track,String filename){

        try(FileWriter writer = new FileWriter(filename,true)) {
            Scanner scan=new Scanner(new File(filename));
            if(scan.hasNext())
                writer.write("\n"+track.toFileWrite());
            else
                writer.write(track.toFileWrite());
        }
        catch (IOException ex){
            System.out.println(ex.getMessage());
        }
    }

}
